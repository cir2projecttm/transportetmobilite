function tempRender(game){

    //game.debug.text(`Speed. x = ${player.body.velocity.x}, y = ${player.body.velocity.y}`, 2, 80, "#f54242");
    //game.debug.text(`Position. x = ${player.isoX}, y = ${player.isoY}`, 2, 58, "#f54242")
    //game.debug.text(game.time.fps || '--', 2, 14, "#aaa");
    renderPlayerSpeed(game);
    renderPlayerDistDone(game);
    renderPlayerPtsLicence(game);
    renderPlayerFine(game);
    return;
}

function renderPlayerSpeed(game){

   let speed = Math.abs(player.body.velocity.y) / 4.1
   //game.debug.text(`speed =  ${speed}`,2,96)
 //   needle.angle = -120 + speed

   if (speed < 120){
      needle.angle = -120 + speed
   }
   else{
      needle.angle = speed -120
   }
   return;
}

function renderPlayerDistDone(game){
   distDoneGUI.setText(""+distDone/100+" \n KM");
   return
}

function renderPlayerPtsLicence(game){
    
   if(ptsLicence<0)
       ptsLicence=0;
   ptsLicenceGUI.setText(""+ ptsLicence +"");
   return
}

function renderPlayerFine(game){
   totalFineGUI.setText("" + totalFine + "€");
   return
}
