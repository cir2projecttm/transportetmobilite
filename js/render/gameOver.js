

function gameOver(){

      console.log(game);
    
      isGameOver = true;
      player.body.velocity.y = 0;
      score = distDone * 10 - totalFine * 1.5;
      score = Math.round(score*10)/10

      /*let styleDeath = {font: "64px Arial",fill :"#000000",stroke: "#FFFFFF",strokeThickness:5,align:"center"};
      let styleComment = {font: "32px Arial",fill :"#000000",stroke: "#FFFFFF",strokeThickness:5,align:"center"};
      let styleRestart = {font: "32px Arial",fill :"#000000",stroke: "#FFFFFF",strokeThickness:1,align:"center"};*/


      //Death screen
      endHud = game.add.graphics(0,0);
      endHud.beginFill(0x000000);
      endHud.lineStyle(2,0x888888,1);
      rect = endHud.drawRect(0,0,width,height); //x,y,larg,hauteur
      guiGroup.add(rect)
      rect.fixedToCamera = true;
      rect.alpha = 0.75

      /*//Death text
      death_txt = game.add.text(width/2,height/2.5, "Game Over \n Score : " + score +"", styleDeath);
      death_txt.anchor.setTo(0.5)
      death_txt.alpha = 0;
      game.add.tween(death_txt).to({alpha:1},2000,"Linear",true);
      guiGroup.add(death_txt);
      death_txt.fixedToCamera = true;

      //Comment text
      comment = (score < 1000) ? "Vous êtes un vrai danger publique\nVous devriez envisager un\nStage de sensibilisation à la sécurité routière" :
         (score < 5000) ? "Attention !\nLa route n'est pas un terrain de jeu\nIl serait dommage de faucher à la place de la faucheuse" :
         (score < 10000) ? "Une conduite exemplaire !\nMais encore des efforts à faire" :
         "Pas mal !\nEn voilà un qui sait faire la différence entre route et circuit !";
      comment_txt = game.add.text(width/2,height/4, comment ,styleComment);
      comment_txt.anchor.setTo(0.5)
      comment_txt.alpha = 0;
      game.add.tween(comment_txt).to({alpha:1}, 2000, "Linear", true);
      guiGroup.add(comment_txt);
      comment_txt.fixedToCamera = true;



      //Restart text beneath the death text
      restart_txt = game.add.text(width/2,height/1.7, "Restarting...",styleRestart);
      restart_txt.anchor.setTo(0.5)
      restart_txt.alpha = 0;
      game.add.tween(restart_txt).to({alpha:1},2000,"Linear",true);
      guiGroup.add(restart_txt);
      restart_txt.fixedToCamera = true;*/


      t1 = (score < 1000) ? "Vous êtes un vrai danger public\nVous devriez envisager un\nStage de sensibilisation à la sécurité routière" :
         (score < 5000) ? "Attention !\nLa route n'est pas un terrain de jeu\nIl serait dommage de faucher à la place de la faucheuse" :
         (score < 10000) ? "Une conduite exemplaire !\nMais encore des efforts à faire" :
            "Pas mal !\nEn voilà un qui sait faire la différence entre route et circuit !";
      t2 = "Game Over \n Score : " + score + "";
      t3 = "Restarting...";


      text1 = game.add.text(width / 2, height / 8, t1);
      text1.fill = "#ffffff";
      text1.anchor.set(0.5, 0.5);
      text1.font = "Poppins";
      text1.fontSize = 32;
      text1.align = 'center';
      guiGroup.add(text1);
      text1.fixedToCamera = true;


      text2 = game.add.text(width / 2, height / 2.5, t2);
      text2.fill = "#ff0000";
      text2.stroke = "#ffff00";
      text2.strokeThickness = 3;
      text2.anchor.set(0.5, 0.5);
      text2.font = "Poppins";
      text2.fontSize = 64;
      text2.align = 'center';
      guiGroup.add(text2);
      text2.fixedToCamera = true;



      text3 = game.add.text(width / 2, height / 1.5, t3);
      text3.fill = "#fff";
      text3.anchor.set(0.5, 0.5);
      text3.font = "Poppins";
      text3.fontSize = 46;
      text3.align = 'center';
      guiGroup.add(text3);
      text3.fixedToCamera = true;

      setTimeout(returnToMainMenu, 10000);
   }



function returnToMainMenu(){
   location.reload();
}
