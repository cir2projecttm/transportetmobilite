function speedLimitUpdate(game){


    if (cooldoownSpeedlimitCheck && player.body.velocity.y < listSpeedLimits[currentSpeedLimit]*-4.4){

      currentSpeedLimit = randNum(4)


      spriteLimit = game.add.sprite(width*0.85,height*0.55,`speedLimit${listSpeedLimits[currentSpeedLimit]}`);
      spriteLimit.fixedToCamera = true;
      guiGroup.add(spriteLimit);

      showEventFailedInfo(game,"Excès de vitesse",2,150);
      ptsLicence -= 2;
      totalFine += 135;
      cooldoownSpeedlimitCheck = false;
      setTimeout(resetCooldownSpeedlimitCheck,7000);
   }
}


 function resetCooldownSpeedlimitCheck(){
   cooldoownSpeedlimitCheck = true;
}
