function tempUpdate(game, getBiome){

    // add control and animation linked here //

    /////////////////////////////////////////////////
    // Used that the car doesn't back up too much. //
    /////////////////////////////////////////////////

    if (player.isoY < maxDistPlayer) {

        maxDistPlayer = player.isoY;
        score++;
    }
    else if (player.isoY >= maxDistPlayer + 400) { // Maximum distance for backing up.
        if (player.body.velocity.y > 0) { // If the player wants to back out.
            player.body.velocity.y = 0;
        }
    }

    /*-------------------------------------------------------------------*/
    /*------------------Mouvement related updates------------------------*/
   /*-------------------------------------------------------------------*/
    ////////////////////////////////////
    //             Braking            //
    ////////////////////////////////////
    var space = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    if (space.isDown) {
        if (player.body.velocity.x < 5 && player.body.velocity.x > -5)
            player.body.velocity.x = 0;

        else if (player.body.velocity.x > 0)
            player.body.velocity.x -= 5;
        else if (player.body.velocity.x < 0)
            player.body.velocity.x += 5;

        if (player.body.velocity.y < 5 && player.body.velocity.y > -5)
            player.body.velocity.y = 0;
        else if (player.body.velocity.y > 0)
            player.body.velocity.y -= 5 + 0.1*player.body.velocity.y;
        else if (player.body.velocity.y < 0)
            player.body.velocity.y += 5;
    }

    ////////////////////////////////////
    //            Turning             //
    ////////////////////////////////////
    if(player.body.velocity.y < -500 || player.body.velocity.y > 500){
       if (player.turningDirection == 'left'){
         player.body.velocity.x = - 8000/( - player.body.velocity.y);
      }else if (player.turningDirection == "none"){
         player.body.velocity.x = 0
      }else{
         player.body.velocity.x = 8000/( - player.body.velocity.y);
      }
   }
   else if ( Math.abs(player.body.velocity.y) < 500 && Math.abs(player.body.velocity.y) > 15){
       if (player.turningDirection == 'left'){
         player.body.velocity.x = -30;
      }else if (player.turningDirection == "none"){
         player.body.velocity.x = 0
      }else{
         player.body.velocity.x = 30;
      }
   }
   else{
      player.body.velocity.x = 0
   }

    ////////////////////////////////////
    //     Player above speedlimit    //
    ////////////////////////////////////

    speedLimitUpdate(game)

    ////////////////////////////////////
    //     Player Not in line   //
    ////////////////////////////////////7


    ////////////////////////////////////
    // Generates the map continuously //
    ////////////////////////////////////

    // Used after a game loop after a new generation
    if (isoMoved > 0) {
        isoMoved--;

        if (isoMoved == 0) {
            for (isoSprite of isoSpriteArray) {
                if (isoSprite.body != null)
                    isoSprite.body.moves = true;
            }
        }
    }



    //__________________________________________________________New Generation___________
    if (player.isoY < posToTpMap) {

        distDone += 5 // Adding Scoring value

        // _____Variables_____
        const firstRow = 0; // xx
        const lastRow = floorTileMapSize - 1; // yy

        const isItAPerpendicularTileRoad = (floorTilesMap[0][lastRow - 1].perpendicularRoad == false) ? !randNum(10) : false;


        let newMap = Array.from(
            new Array(floorTileMapSize), () => Array.from(
                new Array(floorTileMapSize), () => undefined))


        // ____Generate next map in 3 steps_____
        for (let xx = firstRow; xx <= lastRow; xx++) {

            // 1. Destroy bottom tiles
            floorTilesMap[xx][lastRow].destroy(); // Destroy the Plugin.IsoSprite() and it child(s)

            // 2. Add and generate new top tiles
            const posX = tileSize * (xx + 1);
            const biome = getBiome(xx);
            newMap[xx][firstRow] = new FloorTile(posX, 0, areas.road.includes(xx), biome, isItAPerpendicularTileRoad);

            // 3. Move each tiles from one bottom unit in the new map array (not in the game world)
            for (let yy = floorTileMapSize - 1; yy > 0; yy--)
                newMap[xx][yy] = floorTilesMap[xx][yy - 1];
        }


        // _____Destroying old cars_____
        for(let i = 0; i < carIsoArray.length; i++) {

            let car = carIsoArray[i].isoSprite.isoSprite;

            if (car.isoY > mapSize - tileSize * 1.1 || // condition 1 (bottom of the map)
                (car.isoY < tileSize * 0.8 &&
                    player.body.velocity.y > car.body.velocity.y)) { // condition 2 (top of the map)

                car.destroy();
            }
        }


        // _______________Set new generated map_____
        floorTilesMap = newMap; // Erase old array with the new one.

        for (isoSprite of isoSpriteArray) { // Move each tiles to keep them in world borders.

            if (isoSprite.body != null)
                isoSprite.body.moves = false;

            isoSprite.isoY += tileSize;
        }
        isoMoved = 3; // Used specifically to move entities.

        maxDistPlayer = player.isoY;




        // _______________Generate new cars_____
        let coordArray = new Array(); //détermine les positions des routes

        for (let column of areas.road)
            coordArray.push((column + 1 - 0.5) * tileSize - 45);

        let newEntity;
        let posX;
        let max = 0;
        let posY;
        let direction;

        for (let xx = firstRow; xx <= lastRow; xx++) {

            if (floorTilesMap[xx][0].road) {

                max = (xx == mid) ? 5 : 3;

                if (!randNum(max)) {

                    if (!randNum(2)) {
                        posX = tileSize * (xx + 1 - 0.55);
                        direction = Direction.north;
                    }
                    else {
                        posX = tileSize * (xx + 1 - 0.9);
                        direction = Direction.south;
                    }

                    posY = randNum(tileSize);

                    newEntity = new Car(posX, posY, direction);
                }
            }
        }
    }

    game.physics.isoArcade.collide(obstacleGroup);
    game.iso.topologicalSort(obstacleGroup);

    return;
}


function DidThePlayerDubbed(){
    
    
}

//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
function updateBotSpeed(game) {

    //first, we update lilo on every crossingline

    for(let column of floorTilesMap){
        for(let elem of column){
            if(elem._event instanceof CrossingLine)
                elem._event.shouldWeMoveLilo();
        }
    }



    /*
    for (let column of areas.road){
        coordArray.push((column + 1) * tileSize - 0.5 * tileSize - 45);
    }
    */

    for(vehicle of carIsoArray) {

        let bodyBot = vehicle.isoSprite.isoSprite.body;

        vehicle.update(); // update speed in fonction of

        if (shouldItSlow(vehicle, game)) stopCar(vehicle); // Si le véhicule doit ralentir pour pas colisioner

        //_______________________________________________________________
        // Si c'est une voiture (!= character)
        if (vehicle instanceof Car) {
            if (shouldItStop(vehicle)) stopCar(vehicle); // Si on doit s'arreter

            if (vehicle.direction == Direction.south) {
                if      (bodyBot.velocity.y > 200) bodyBot.velocity.y -= 10;
                else if (bodyBot.velocity.y < 180) bodyBot.velocity.y += 10;
            }

            else if (vehicle.direction == Direction.north) {
                if      (bodyBot.velocity.y < -200) bodyBot.velocity.y += 10;
                else if (bodyBot.velocity.y > -180) bodyBot.velocity.y -= 10;
            }
        }
        //_______________________________________________________________
        // Si c'est un bateau (!= character)
        else if (vehicle instanceof Boat) {

            if (vehicle.direction == Direction.south) {
                if      (bodyBot.velocity.y > 120) bodyBot.velocity.y -= 2;
                else if (bodyBot.velocity.y < 100) bodyBot.velocity.y += 2;
            }

            else if (vehicle.direction == Direction.north) {
                if      (bodyBot.velocity.y < -120) bodyBot.velocity.y += 2;
                else if (bodyBot.velocity.y > -100) bodyBot.velocity.y -= 2;
            }
        }
        //_______________________________________________________________
        // Si c'est un train (!= character)
        else if (vehicle instanceof Train) {

            if (vehicle.direction == Direction.south) {
                if      (bodyBot.velocity.y > 250) bodyBot.velocity.y -= 5;
                else if (bodyBot.velocity.y < 230) bodyBot.velocity.y += 5;
            }

            else if (vehicle.direction == Direction.north) {
                if      (bodyBot.velocity.y < -250) bodyBot.velocity.y += 5;
                else if (bodyBot.velocity.y > -230) bodyBot.velocity.y -= 5;
            }
        }
        //_______________________________________________________________
        // Si c'est un character (!= vehicule)
        else if(vehicle instanceof WalkingCharacter){

            //characterOverlap(vehicle,game);

            if (!vehicle.isStoppedForever && shouldCharacterStopForever(vehicle, game)) {
                vehicle.isStoppedForever = true;
                vehicle.isoSprite.isoSprite.body.moves = 0;
            }

            // Si on va vers le bas
            else if (vehicle.direction == Direction.south) {

                if (vehicle.isoSprite.isoSprite.body.velocity.y > 50)
                    vehicle.isoSprite.isoSprite.body.velocity.y -= 5;

                else if (vehicle.isoSprite.isoSprite.body.velocity.y < 40)
                    vehicle.isoSprite.isoSprite.body.velocity.y += 5;

            }

            // Si on va vers le haut
            else if (vehicle.direction == Direction.north) {

                if (vehicle.isoSprite.isoSprite.body.velocity.y < -50)
                  vehicle.isoSprite.isoSprite.body.velocity.y += 5;

                else if (vehicle.isoSprite.isoSprite.body.velocity.y > -40)
                    vehicle.isoSprite.isoSprite.body.velocity.y -= 5;

            }
        }
    }
}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


function characterOverlap(character,game){

    for(elem of carIsoArray){
        if(elem instanceof WalkingCharacter){

            game.physics.isoArcade.overlap(character.isoSprite.isoSprite, elem.isoSprite.isoSprite ,
           function(){
                character.isoSprite.isoSprite.destroy();
                //character.isoSprite.isoSprite.body.velocity.x += randNum(100);
            });

        }

    }

}
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function stopCar(car) {


        if (Math.abs(car.isoSprite.isoSprite.body.velocity.y) <= 5)
            car.isoSprite.isoSprite.body.velocity.y = 0;

        else if (car.direction == Direction.south)
            while (car.isoSprite.isoSprite.body.velocity.y  > 0)
                car.isoSprite.isoSprite.body.velocity.y -= 5;

        else (car.direction == Direction.north)
            while (car.isoSprite.isoSprite.body.velocity.y  < 0)
                car.isoSprite.isoSprite.body.velocity.y += 5;

}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function shouldCharacterStopForever(character, game) {

    /*
     coordX = ((x + 50) / 200 >> 0);
     coordY = ((y + 50) / 200 >> 0);
    */

    const x = character.isoSprite.isoSprite.isoX;
    const y = character.isoSprite.isoSprite.isoY;


    let xx = ((x + 50) / 200 >> 0);
    let yy = ((y + 50) / 200 >> 0);

    if(character.direction == 'south' && !(floorTilesMap[xx][yy+1] === undefined) && floorTilesMap[xx][yy+1].perpendicularRoad )
        return true;


    if(character.direction == 'north' &&!(floorTilesMap[xx][yy-1] === undefined) &&floorTilesMap[xx][yy-1].perpendicularRoad)
        return true;

}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function shouldItStop(car) {

    const isoCar = car.isoSprite.isoSprite;
    const tileCar = {
        x: (isoCar.isoX + 50) / 200 >> 0,
        y: (isoCar.isoY + 50) / 200 >> 0
    };


    if (car.direction == Direction.north || car.direction == Direction.south) // N/S car direction
        for (let y = tileCar.y - 1; y <= tileCar.y + 1; y++)
            if (y >= 0 && y < floorTilesMap.length) // Security out of range
                return floorTilesMap[tileCar.x][y].shouldBotStop(car);

    else if (car.direction == Direction.est || car.direction == Direction.west) // E/W car direction
        for (let x = tileCar.x - 1; x <= tileCar.x + 1; x++)
            if (x >= 0 && x < floorTilesMap.length) // Security out of range
                return floorTilesMap[x][tileCar.y].shouldBotStop(car);


    return false;

}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function isThePlayerBehind(bot, game){

    if (bot.isoSprite.isoSprite.isoY > player.isoY) {
        let dist = game.physics.isoArcade.distanceBetween(bot.isoSprite.isoSprite,player);
        if (dist < 250)
            return true;
    }
    return false;
}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function isThePlayerInFront(bot,game){

    if(bot.isoSprite.isoSprite.isoY < player.isoY){
        let distY = Math.abs(Math.abs(bot.isoSprite.isoSprite.isoY)-player.isoY)
        let distX = Math.abs(Math.abs(bot.isoSprite.isoSprite.isoX)-player.isoX)
        if(distY < 300 && distX <20)
            return true;
    }
}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function shouldItSlow(bot1,game){

    let dist = 0;

    //regarde le player
    if(bot1 instanceof Car && bot1.direction == 'north' && isThePlayerBehind(bot1,game)){
        return true;
    }
    else if(bot1 instanceof Car && bot1.direction == 'south' && isThePlayerInFront(bot1,game)){
        return true;
    }

    //regarde les autres bot
    for(let bot2 of carIsoArray)
        if(bot2.type == bot1.type && bot2 != bot1)

            if(bot2.direction == bot1.direction){
                if(bot1.direction == 'south'){

                    if(bot2.isoSprite.isoSprite.isoY > bot1.isoSprite.isoSprite.isoY){
                        dist = game.physics.isoArcade.distanceBetween(bot1.isoSprite.isoSprite,bot2.isoSprite.isoSprite);
                        if(dist < 150)
                            return true;
                    }
                }
                else{

                    if(bot2.isoSprite.isoSprite.isoY < bot1.isoSprite.isoSprite.isoY){
                        dist = game.physics.isoArcade.distanceBetween(bot1.isoSprite.isoSprite,bot2.isoSprite.isoSprite);
                        if(dist < 150)
                            return true;
                    }


                }
            }

    return false;
}


//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function slowCar(car){

    while(shouldItSlow(car)){


        if(car.direction == Direction.south){
            while(car.isoSprite.isoSprite.body.velocity.y  > 10){
                car.isoSprite.isoSprite.body.velocity.y -= 10;
            }
        }
        else{
            while(car.isoSprite.isoSprite.body.velocity.y  < 10){
                car.isoSprite.isoSprite.body.velocity.y += 10;
            }
        }
    }
}

function didWeCrashedaPedestrian(game){
    
    
    for(let elem of waitingCharacterTab){
        game.physics.isoArcade.overlap(elem.isoSprite.isoSprite, player ,function(){
            if(!alreadyCrashedSomeone){
                game.crash.play();
                console.log("waiting character shooted")
                
                elem.isoSprite.isoSprite.animations.play('dead')
                elem.isoSprite.isoSprite.body.angularDrag = 100;
                elem.isoSprite.isoSprite.body.angularVelocity = 100;
                ///elem.isoSprite.isoSprite.body.drag = (100,100,100); add variable to dont move anithing else
                player.body.angularDrag = 100;
                player.body.angularVelocity = -50;
                player.body.drag.set(100,100,100);

                gameOver(game);

                //alexis balance ton sang ici
                
                //ne marche pas :
                
                //setTimeout(function(){ alert("Hello"); } , 2000);
                //setTimeout(callGameOver() , 2000);

                //gameOver(game);
                setTimeout(gameOver, 1000);
                alreadyCrashedSomeone = true;
            }
        });
    }
}

//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//————————————————————————————————————————————————————————————————————————————
function didWeCrashedAnotherCar(game){

    for(let elem of carIsoArray){
        game.physics.isoArcade.overlap(elem.isoSprite.isoSprite, player ,function(){
            if(!alreadyCrashedSomeone){
                game.crash.play();
                
                
                if (elem instanceof WalkingCharacter) 
                    elem.isoSprite.isoSprite.animations.play('dead')

                elem.isoSprite.isoSprite.body.angularDrag = 100;
                elem.isoSprite.isoSprite.body.angularVelocity = 100;
                ///elem.isoSprite.isoSprite.body.drag = (100,100,100); add variable to dont move anithing else
                player.body.angularDrag = 100;
                player.body.angularVelocity = -50;
                player.body.drag.set(100,100,100);
                
                //alexis balance ton sang ici
                
                
                //gameOver(game);
                setTimeout(gameOver, 1000);
                alreadyCrashedSomeone = true;
            }
        });
    }
}
