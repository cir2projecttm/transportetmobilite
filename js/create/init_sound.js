function initSound(game){
    
    game.startingCar = game.sound.add('startingCar');
    game.crash = game.sound.add('crash');
    
    game.mainThemeMusic = game.sound.add('mainTheme');
    game.menuMusic = game.sound.add('menu');
    
    game.startingCar.volume = 0.2;
    game.startingCar.play();
        
    
    game.mainThemeMusic.volume = 0.3;
    game.mainThemeMusic.loop = true;
    game.mainThemeMusic.play();
    
}
