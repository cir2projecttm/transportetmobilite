function initPlayer(game,color){

    ////////////////////////////////
    //  def and init player here  //
    ////////////////////////////////

    // Create the player
    let posX = mid * tileSize + 90;
    let posY = (mid + 1) * tileSize;
    let playerCar = 'carnorth'+color;

    player = game.add.isoSprite(posX, posY, 0, playerCar, 0, obstacleGroup);

    player.anchor.set(0.5, 1);
    game.physics.isoArcade.enable(player);
    player.body.collideWorldBounds = true;
    player.turningDirection = "none"
    isoSpriteArray.push(player); // Array used to continuous map generation

    maxDistPlayer = player.isoY; // Used that the car doesn't back up too much.
    posToTpMap = player.isoY - tileSize; // Used to generate the map continuously if the player advances.

    game.camera.follow(player); // Make the camera follow the 'player'.

    return;
}
