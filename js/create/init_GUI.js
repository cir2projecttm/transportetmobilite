function initGUI(game){

   diag = Math.sqrt(width * width + height * height)

   let styleDist = {font: "48px Poppins",fill :"#F5A83D",stroke: "#0A0A0A",strokeThickness:5}
   let styleLicence = {font: "48px Poppins",fill :"#2431ED",stroke: "#0A0A0A",strokeThickness:5}
   let styleFine = {font: "48px Poppins",fill :"#B32D45",stroke: "#0A0A0A",strokeThickness:5}


   //------Overall GUI Components------
   hud = game.add.graphics(0,0);

   hud.beginFill(0x445063);
   hud.lineStyle(2,0x888888,1);

   bkgRectX    = 0
   bkgRectY    = height*0.75
   bkgCircleX  = width*0.15
   bkgCircleY  = height*0.85

   bkgRect = hud.drawRect(bkgRectX,bkgRectY,width,height*0.25); //x,y,larg,hauteur
   bkgCircle = hud.drawCircle(bkgCircleX,bkgCircleY,diag * 0.25);

   guiGroup.add(bkgRect);
   guiGroup.add(bkgCircle);


   //-----Speed Components--------
   odometer = game.add.sprite(0,0,'odometer');
   needle   = game.add.sprite(0,0,'needle');

   //-----Scoring Components------
   licence      = game.add.sprite(0,0,'licence');
   fine         = game.add.sprite(0,0,'fine');
   distDoneGUI  = game.add.text(0,0,0, styleDist);
   ptsLicenceGUI= game.add.text(0,0,12,styleLicence);
   totalFineGUI = game.add.text(0,0,0, styleFine);



   guiGroup.add(odometer);
   guiGroup.add(needle);
   guiGroup.add(licence);
   guiGroup.add(fine);
   guiGroup.add(distDoneGUI);
   guiGroup.add(ptsLicenceGUI);
   guiGroup.add(totalFineGUI);


   odometer.scale.setTo(    diag*0.00038,diag*0.00038);
   needle.scale.setTo(      diag*0.0005,diag*0.0005);
   licence.scale.setTo(     diag*0.00025,diag*0.00025);
   fine.scale.setTo(        diag*0.00025,diag*0.00025);
   distDoneGUI.scale.setTo( diag*0.0007,diag*0.0007);
   ptsLicenceGUI.scale.setTo( diag*0.0007,diag*0.0007);
   totalFineGUI.scale.setTo( diag*0.00051,diag*0.00051);



   needle.anchor.setTo(0.5,1.2);
   odometer.anchor.setTo(0.5,0.5);
   licence.anchor.setTo(0.5,0.5);
   fine.anchor.setTo(0.5,0.5);
   distDoneGUI.anchor.setTo(0.5,0.5);
   ptsLicenceGUI.anchor.setTo(0.5,0.5);
   totalFineGUI.anchor.setTo(0.5,0.5);

   odometer.position.setTo(bkgCircleX ,bkgCircleY - height*0.07)
   needle.position.setTo(bkgCircleX,bkgCircleY)
   licence.position.setTo(bkgRectX + bkgRect.width * 0.55 ,bkgRectY + bkgRect.height * 0.12 );
   fine.position.setTo(bkgRectX + bkgRect.width * 0.82 ,bkgRectY + bkgRect.height *0.12 );
   distDoneGUI.position.setTo(bkgRectX + bkgRect.width * 0.38 ,bkgRectY + bkgRect.height *0.22 );
   ptsLicenceGUI.position.setTo(licence.x + licence.width * 0.1, licence.y );
   totalFineGUI.position.setTo(fine.x + fine.width * 0.15 , fine.y - fine.height * 0.08);

   odometer.fixedToCamera = true;
   needle.fixedToCamera = true;
   licence.fixedToCamera = true;
   fine.fixedToCamera = true;
   distDoneGUI.fixedToCamera = true;
   ptsLicenceGUI.fixedToCamera = true;
   totalFineGUI.fixedToCamera = true;




   bkgCircle.fixedToCamera = true;
   bkgRect.fixedToCamera = true;


   odometer.bringToTop();
   needle.bringToTop();
   licence.bringToTop();
   fine.bringToTop();
   distDoneGUI.bringToTop();
   ptsLicenceGUI.bringToTop();
   totalFineGUI.bringToTop();

   //------Speed Limit---------
   spriteLimit = game.add.sprite(width*0.85,height*0.55,`speedLimit${listSpeedLimits[currentSpeedLimit]}`);
   spriteLimit.fixedToCamera = true;
   guiGroup.add(spriteLimit);




   // last time
   title = game.add.text(bkgRectX + bkgRect.width * .82 ,bkgRectY + bkgRect.height * 0.40, "PERFECT DRIVE");
   title.fill = "#ffffff";
   title.anchor.set(0.5, 0.5);
   title.font = "Poppins";
   title.fontSize = 45;
   title.align = 'center';
   guiGroup.add(title);
   title.fixedToCamera = true;

   //distDoneGUI.font = "Poppins";
   distDoneGUI.align = 'center';


   return;
}
