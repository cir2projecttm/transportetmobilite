function initInput(game){

    ///////////////////////////////
    // def and init control here //
    ///////////////////////////////


    // Set up our controls.
    this.cursors = game.input.keyboard.createCursorKeys();
    this.game.input.keyboard.addKeyCapture([
        Phaser.Keyboard.W,
        Phaser.Keyboard.A,
        Phaser.Keyboard.S,
        Phaser.Keyboard.D,
        Phaser.Keyboard.UP,
        Phaser.Keyboard.DOWN,
        Phaser.Keyboard.LEFT,
        Phaser.Keyboard.RIGHT,
        Phaser.Keyboard.SPACEBAR,
        Phaser.Keyboard.ESC
    ]);


    // directions
    var w = game.input.keyboard.addKey(Phaser.Keyboard.W);
    var a = game.input.keyboard.addKey(Phaser.Keyboard.A);
    var s = game.input.keyboard.addKey(Phaser.Keyboard.S);
    var d = game.input.keyboard.addKey(Phaser.Keyboard.D);
    var up = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    var down = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    var left = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    var right = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    var space = game.input.keyboard.addKey(Phaser.Keyboard.SPACE);
    var escape = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

    w.onDown.add(goForward, this);
    a.onDown.add(goLeft, this);
    s.onDown.add(goBackward, this);
    d.onDown.add(goRight, this);


    up.onDown.add(goForward, this);
    down.onDown.add(goBackward, this);
    left.onDown.add(goLeft, this);
    right.onDown.add(goRight, this);

    escape.onDown.add(function(){
       if(game.paused == false){

          setTimeout(function(){game.paused = true;},50)

          //Pause screen
          pauseHud = game.add.graphics(0,0);
          pauseHud.beginFill(0x000000);
          pauseHud.lineStyle(2,0x888888,1);
          rect = pauseHud.drawRect(0,0,width,height); //x,y,larg,hauteur
          guiGroup.add(rect)
          rect.fixedToCamera = true;
          rect.alpha = 0.75

          //showEventFailedInfo(game,"test",1,2)

          //Pause text
          let stylePause = {font: "64px Arial",fill :"#000000",stroke: "#FFFFFF",strokeThickness:5,align:"center"};
          pauseTxt = game.add.text(width/2,height*0.2, "Pause",stylePause);
          pauseTxt.anchor.setTo(0.5)
          guiGroup.add(pauseTxt);
          pauseTxt.fixedToCamera = true;

          //Return to main menu text
          mmTxt = game.add.sprite(width*0.5,height*0.5,'mainMenu');
          mmTxt.scale.setTo(0.5)
          mmTxt.anchor.setTo(0.5)
          guiGroup.add(mmTxt);
          mmTxt.fixedToCamera = true;

          game.input.onDown.add(function(){
             x1 = width*0.5 - mmTxt.width*0.5,    x2 = width*0.5 + mmTxt.width*0.5
             y1 = height*0.5 - mmTxt.height*0.5,  y2 = height*0.5 + mmTxt.height*0.5
             if((event.x > x1) && (event.y> y1) && (event.x< x2) && (event.y<y2 )){
               location.reload()
             }
          },self)

           //Return back to Game
           btgTxt = game.add.sprite(width*0.5,height*0.7,'backToGame');
           btgTxt.scale.setTo(0.5)
           btgTxt.anchor.setTo(0.5)
           guiGroup.add(btgTxt);
           btgTxt.fixedToCamera = true;
           game.input.onDown.add(function(){
             x1 = width*0.5 - btgTxt.width*0.5,    x2 = width*0.5 + btgTxt.width*0.5
             y1 = height*0.7 - btgTxt.height*0.5,  y2 = height*0.7 + btgTxt.height*0.5
             if((event.x > x1) && (event.y> y1) && (event.x< x2) && (event.y<y2 )){
                 pauseHud.destroy()
                 rect.destroy()
                 pauseTxt.destroy()
                 mmTxt.destroy()
                 btgTxt.destroy()
                 game.paused = false;
              }
           },this)
       }
       else{
          pauseHud.destroy()
          rect.destroy()
          pauseTxt.destroy()
          mmTxt.destroy()
          btgTxt.destroy()
          game.paused = false;
       }
    }
, this);


    function goForward(){

        if (player.body.velocity.y > -840) {
                player.body.velocity.y -= 30;
        }

   }

   function goLeft(){
      if (player.turningDirection == "none"){
         player.turningDirection = "left";
      }
      else if (player.turningDirection == "right"){
         player.turningDirection = "none"
      }

   }

   function goRight(){
      if (player.turningDirection == "none"){
         player.turningDirection = "right";
      }
      else if (player.turningDirection == "left"){
         player.turningDirection = "none"
      }

   }

   function goBackward(){
     if (player.isoY < maxDistPlayer + 400) { // Used that the car doesn't back up too much.
         if (player.body.velocity.y < 90 && player.body.velocity.y > -30) {
             player.body.velocity.y += 30;
         }
     }
   }



    return;
}
