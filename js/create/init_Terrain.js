function initTerrain(game,getBiome){

    // set the Background color of our game
    game.stage.backgroundColor = "0x344233"; //HEX

    // create groups for different tiles
    floorGroup       = game.add.group();
    onFloorGroup     = game.add.group();
    obstacleGroup    = game.add.group();
    player           = game.add.group();
    guiGroup         = game.add.group();

    // Set the gravity in our game
    game.physics.isoArcade.gravity.setTo(0, 0, -500);



    // _____Define y column areas_____
    const seaLimit = Math.floor(floorTileMapSize * 0.6);

    // Feed areas
    for (let column = 0; column < floorTileMapSize; column++) {

        if (column == seaLimit) // Beach
            areas.beach.push(column);

        else if (column > seaLimit) // Sea
            areas.sea.push(column);

        else if (column == mid + 1) // neutral
            areas.neutral.push(column);

        else if (column % 3 == mid % 3) // Road
            areas.road.push(column);

        else if (!randNum(Math.floor(floorTileMapSize / 3))) // Train
            areas.train.push(column);

        else if (!randNum(Math.floor(floorTileMapSize / 3))) // River
            areas.river.push(column);

        else // Decor
            areas.decor.push(column);
    }
    //console.log("area " , areas)

    

    // _____Create the floor tiles_____

    for (let yy = 0; yy < floorTileMapSize; yy++) {

        const isItAPerpendicularTileRoad = (yy > 0 && floorTilesMap[0][yy - 1].perpendicularRoad == false) ? !randNum(10) : false;

        for (let xx = 0; xx < floorTileMapSize; xx++) {

            const posX = tileSize * (xx + 1);
            const posY = tileSize * (yy + 1);
            const biome = getBiome(xx);

            floorTilesMap[xx][yy] = new FloorTile(
                posX, posY, areas.road.includes(xx), biome, isItAPerpendicularTileRoad); // Settings set in the instance
        }
    }



    // _____Create movible other cars (entity) tile_____
    var movibleEntityTile;
    for (let xx = 0; xx < floorTileMapSize; xx++) {
        for (let yy = 0; yy < floorTileMapSize; yy++) {

            if (yy != mid && yy != mid + 1) {

                if (areas.road.includes(xx)) {
    
                    rnd = randNum(4);
                    
                    if (!rnd) {
    
                        let xxx = tileSize * (xx + 1) - 110;
                        let yyy = tileSize * (yy + 1);
                        
                        movibleEntityTile = new Car(xxx, yyy, Direction.north);
    
                        carIsoArray.push(movibleEntityTile);
                    }
                }
            }
        }
    }

    return;
}
