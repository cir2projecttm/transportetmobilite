const buttons = document.querySelectorAll('a');
buttons.forEach(btn => {
    btn.addEventListener('click', function(b) {

        let x = b.clientX - b.target.offsetLeft;
        let y = b.clientY - b.target.offsetTop;

        let ripples = document.createElement('span');
        ripples.style.left = x + 'px';
        ripples.style.top = y + 'px';
        this.appendChild(ripples);

        setTimeout(() => {
            ripples.remove()
        }, 1000);
    })
})



function initMenu(){
    let left = document.getElementById("caseLeft");
    let selectedCar = document.getElementById("selectedCar");
    let selectedCarText = document.getElementById("selectedCarText");
    let right = document.getElementById("caseRight");
    let run = document.getElementById("casePlay");
    let nombre = randNum(10);
    let voitures = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
    
    let loadGameWindow = function(){ //s'occupe de l'affichage de la fenêtre de jeu (action pas réversible)
        /*let menuListToDelete = document.getElementById("menu");
        menuListToDelete.remove();*/
        
        let head = document.getElementsByTagName('HEAD')[0];

        let link = document.createElement('link');

        // set the attributes for link element
        link.rel = 'stylesheet';

        link.type = 'text/css';

        link.href = 'css/styleGameWindow.css';

        //Append link element to HTML head
        head.appendChild(link);

    }
    let affichageMidBox= function(){ //s'occupe de l'affichage de la voiture
        let link = "assets/tiles/car/carnorth"+voitures[nombre]+".png";
        selectedCar.setAttribute ("src",link);
        //selectedCarText.innerHTML = voitures[nombre]+" car"; //debug
    }
    affichageMidBox();
    
    /*  Listeners sur les différents boutons*/
    left.addEventListener("click",function(){
        nombre--;
        if(nombre == -1){
            nombre= voitures.length -1;
        }
        affichageMidBox();
    });
    right.addEventListener("click",function(){
        nombre++;
        if(nombre == voitures.length ){
            nombre=0;
        }
        affichageMidBox();
    });
    run.addEventListener("click",function(){
        color = voitures[nombre];
        loadGameWindow();
        init(color);
    });
}
initMenu();