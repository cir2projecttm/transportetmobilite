//______________________________________________________________________________________________________________//
//______________________________________________________________________________________________________________//
// Class about floor tiles editing for map generation
class FloorTile {


    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------Constructors & initializations--------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    
    //__________________________________________________________________________________________________________//
    constructor(x, y, road, biome = Biome.decor, perpendicularRoad = false) {

        this.init(x, y, road, biome, perpendicularRoad);
    }

    
    //__________________________________________________________________________________________________________//
    init(x, y, road, biome, perpendicularRoad) {

        this.road = road;
        this.biome = biome;
        this.perpendicularRoad = perpendicularRoad;

        this.isoSprite = this.setIsoSprite(x, y, biome, perpendicularRoad);
    }


    //__________________________________________________________________________________________________________//
    setIsoSprite(x, y, biome, perpendicularRoad) {
        // Sets the image and the event according to the biome

        switch(biome) {

            case Biome.road: // Road
                this._img = (!perpendicularRoad) ? `road0`: `road2`;
                this._event = (!perpendicularRoad) ? this.setRoadEvent(x, y) : this.setCrossRoadEvent(x, y);
                //spawn personnage
                if((!perpendicularRoad)){
                    
                    let column = areas.road[areas.road.length-1];
                    let carX = (column + 1) * tileSize - 0.5 * tileSize - 45;

                    //1 si à droite du joueur, 0 si à gauche 
                    let posCharacter = x > carX ? 1 : 2;
                    
                    if((posCharacter==2 && randNum(10)) || (posCharacter==1 && randNum(3))){

                       // let xx = x > carX ? x-30 + randNum(200) : x+20;
                        let characterImg = '';
                        let xx = 0;

                        switch(randNum(4)){
                            case 0:
                                characterImg = 'girlnorth1';
                                xx = x > carX ? x-30 + randNum(200) : x+20;
                                break;
                            case 1:
                                characterImg = 'girlsouth1';
                                xx = x > carX ? x +50 + randNum(200) : x+20;
                                break;
                            case 2:
                                characterImg = 'girlsouth2';
                                xx = x > carX ? x +50 + randNum(200) : x+20;
                                break;
                            case 3: //deuxième easter egg
                                xx = x > carX ? x +100 + randNum(20) : x+20;
                                characterImg = !randNum(35) ? 'stitchsurf' : 'mansouth1';
                                break;
                            default:
                                break;
                        }

                        this.walkingCharacter = new WalkingCharacter(xx,y-50,characterImg);       
                    }
                }
                break;

            case Biome.decor: // Decor
                this._img = (!perpendicularRoad) ? `grass`: `road1`;
                this._event = (!perpendicularRoad) ? this.setDecorEvent(x, y) : undefined;
                break;
            
            case Biome.train: // Train
                this._img = (!perpendicularRoad) ? `trainTracks0`: `trainTracks1`;
                this._event = (!perpendicularRoad) ? this.setTrainEvent(x, y) : undefined;
                break;
        
            case Biome.sea: // Sea
                this._img = `sea`;
                this._event = (!perpendicularRoad) ? this.setBoatEvent(x, y) : undefined;
                break;
        
            case Biome.beach: // Beach
                this._img = (!perpendicularRoad) ? `beach`: `road4`;
                this._event = undefined;
                break;
        
            case Biome.river: // River
                this._img = (!perpendicularRoad) ? `river`: `road3`;
                this._event = undefined;
                if (!perpendicularRoad && !randNum(25)) 
                    new IsoSprite(x - tileSize / 2, y - tileSize / 2, 'stitch', obstacleGroup) // Easter egg :)
                break;
            
            default: // Neutral, None (Basic settings)
                this._img = (!perpendicularRoad) ? `grass`: `road1`;
                this._event = undefined;
        }
        
        return new IsoSprite(x, y, this._img, floorGroup)
    }


   

    /*----------------------------------------------------------------------------------------------------------*/
    /*----------------------------------------------Events setters----------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/
    
    //__________________________________________________________________________________________________________//
    setRoadEvent(x, y) {
        // Generates a random road event
        
        switch (randNum(6)) {
            case 0: return new CrossingLine(x, y);
            case 1: return new Stop(x, y, !randNum(2) ? 1 : 2);
            //case 2: return new TraficLight(x, y,randNum(2) ? 1 : 2);
            default:  return undefined;
        }
    }
    

    //__________________________________________________________________________________________________________//
    setCrossRoadEvent(x, y) {
        // Generates a random crossroad event
        let status = !randNum(2) ? Line.NS : Line.EW;

        return (!randNum(2)) ? new CrossRoad(x, y) : new RightHandPriority(x, y, status);
    }


    //__________________________________________________________________________________________________________//
    setDecorEvent(x, y) {
        // Generates a random decor event

        let tileName = (!randNum(2)) ? `decor${randNum(11).toString()}` : `building${randNum(10).toString()}`;

        return new IsoSprite(x, y, tileName, obstacleGroup)
    }


    //__________________________________________________________________________________________________________//
    setTrainEvent(x, y) {
        // Generates a train event

        const direction = (x % 400) ? `north` : `south` // Pair: `north`, Impair: `south`
        if (!randNum(8))
            new Train(x, y, direction);

        return undefined; // On return undefined psk le train n'est pas relié au la tuile
    }


    //__________________________________________________________________________________________________________//
    setBoatEvent(x, y) {
        // Génère un evènement aléatoire

        const direction = (x % 400) ? `north` : `south` // Pair: `north`, Impair: `south`
        if (!randNum(8))
            new Boat(x, y, direction);

        return undefined; // On return undefined psk le train n'est pas relié au la tuile
    }


   

    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------Entities conditions-------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    shouldBotStop(bot) {
        // Call by a bot

        if (this._event != undefined) return this._event.shouldYouStop(bot);
        return false;
    }


    //__________________________________________________________________________________________________________//
    isPlayerConditionRespected() {
        // Call by THE player

        if (this._event != undefined && this.biome == Biome.road) return this._event.isConditionRespected();
        
        return true;
    }


   
    
    /*----------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------Deconstructor----------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    destroy() {
        // destroy each isoSprit linked to this floorTile

        this.isoSprite.destroy();
        
        if (this._event != undefined)
            this._event.destroy();
    }

    

    /*getMapCoord(x) {
        const adjustment = 50;
        return x + adjustment;  // + 50 => âr rapport au décalage de coordonnée
    }
    getSide() {
        const left = 1;
        const right = 2;
        const x = this.getMapCoord().x;

        //console.log("x:" ,x, "tileSize", tileSize)
        //console.log("x % tileSize : ", x % tileSize, "tileSize / 2 : ", tileSize / 2);
          
        return (x % tileSize < tileSize / 2) ? left : right ;
    }*/
}
//______________________________________________________________________________________________________________//
//______________________________________________________________________________________________________________//