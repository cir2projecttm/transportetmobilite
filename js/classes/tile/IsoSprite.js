//———————————————————————————————————————————————————————————————————————————————————————————
class IsoSprite{

    //___________________________________________________________________________________________________________________________
    constructor(x, y, img, group, physic) {
        this.init(x, y, img, group, physic);
        this.set()
    }

    //___________________________________________________________________________________________________________________________
    init(x, y, img, group, physic) {
        this.physic = physic;
        this.isoSprite = game.add.isoSprite(x, y, 0, img, 0, group);
    }

    //___________________________________________________________________________________________________________________________
    set() {
        this.isoSprite.anchor.set(0.5, 1);

        if (this.physic)
            this.setPhysic()
        
        isoSpriteArray.push(this.isoSprite);
    }

    //___________________________________________________________________________________________________________________________
    setPhysic() {
        game.physics.isoArcade.enable(this.isoSprite);
        this.isoSprite.body.collideWorldBounds = true;

        this.isoSprite.body.bounce.set(0.2, 0.2, 0);
        this.isoSprite.body.drag.set(100, 100, 0);
    }

    //___________________________________________________________________________________________________________________________
    destroy() {
        const foundIndex = isoSpriteArray.indexOf(this.isoSprite);
        isoSpriteArray.splice(foundIndex, 1) // Remove the isoSprite from isoSpriteArray

        this.isoSprite.destroy();
    }
}