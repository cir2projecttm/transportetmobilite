class Boat extends Bot {
    //___________________________________________________________________________________________________________________________
    constructor(x, y, direction) {

        const img = (direction == Direction.north) ? (
            (!randNum(2)) ? `boat0` : `boat2`) : (
            (!randNum(2)) ? `boat1` : `boat3`);
        const adjustment = tileSize * 0.85;
        x -= adjustment;
        y -= adjustment;

        super(x, y, img, direction,'boat')
        
        this.set(direction);

        carIsoArray.push(this);
    }

    //___________________________________________________________________________________________________________________________
    set(direction) {

        let speed = tileSize;
        this.isoSprite.isoSprite.body.velocity.y = (direction == Direction.north) ? -speed : speed;
    }

    //___________________________________________________________________________________________________________________________
    destroy() {
        
        const foundIndex = carIsoArray.indexOf(this);
        carIsoArray.splice(foundIndex, 1) // Remove the isoSprite from carIsoArray

        super.destroy()
    }
}