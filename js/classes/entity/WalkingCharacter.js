class WalkingCharacter extends Bot {


    constructor(x,y,img = 'girlnorth1'){
        
        super(x, y, img, 0,'character');
        this.isoSprite.isoSprite.animations.add('ok', [0], 1, true);
        this.isoSprite.isoSprite.animations.add('dead', [1], 1, true);
        this.isoSprite.isoSprite.animations.play('ok');
        

        this.init(x,y,img);
    }
    
    init(x,y,img){
        
        switch(img){
            case 'girlnorth1':
                this.direction = Direction.north;
                break;
            case 'girlsouth1':
                this.direction = Direction.south;
                break;
            case 'girlsouth2':
                this.direction = Direction.south;
                break;
            case 'mansouth1': 
                this.direction = Direction.south;
                break;
            case 'stitchsurf': 
                this.direction = !randNum(2) ? Direction.south : Direction.north;
                break;
            default:
                break;
        }
        
        
        this.isStoppedForever = false; 
        //pour se simplifier la vie : dès qu'ils croisent un crossRoad ils s'arrêtent de bouger 
        
        carIsoArray.push(this);

    }
    
    destroy() {
        
        const foundIndex = carIsoArray.indexOf(this);
        carIsoArray.splice(foundIndex, 1) // Remove the isoSprite from carIsoArray

        super.destroy()
    }
    
}