class Car extends Bot {
    //___________________________________________________________________________________________________________________________
    constructor(x, y, direction){

        const img = `car${direction}${randNum(11)}`;

        super(x, y, img, direction,'car');
        
        this.init();

        carIsoArray.push(this);
    }
    //___________________________________________________________________________________________________________________________
    init(){
        this.stoped = false;
        this.timePassedWaiting = 0;
    }

    //___________________________________________________________________________________________________________________________
    destroy() {
        const foundIndex = carIsoArray.indexOf(this);
        carIsoArray.splice(foundIndex, 1) // Remove the isoSprite from carIsoArray

        super.destroy()
    }
}