//crée dans le but d'avoir un personnage qui traverse au passage piéton

class WaitingCharacter extends Bot{
    
    constructor(x,y,img = 'lilo'){
        
        super(x, y, img, 0,'character');
        this.isoSprite.isoSprite.animations.add('ok', [0], 1, true);
        this.isoSprite.isoSprite.animations.add('dead', [1], 1, true);
        this.isoSprite.isoSprite.animations.play('ok');
        //carIsoArray.push()
        waitingCharacterTab.push(this);
        this.init(x,y);

    }
    
    init(x,y){
        this.direction = Direction.west;
        this.destX = x - 140;
        this.destY = y - 210;
        this.alreadyPassed=false;
        this.isWalking = false;
    }
    
    
    
    startWalking(){
        this.isWalking = true;
        if(this.isoSprite.isoSprite.isoX > this.destX)
            this.isoSprite.isoSprite.body.velocity.x = -100;    
    }
    
    destroy(){
      //this.isoSprite.isoSprite.destroy();
    }
}