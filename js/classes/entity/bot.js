class Bot {
    
    //_________________________________________________________________________________________
    constructor(x, y, img, direction,type, group = obstacleGroup) {

        this.x = x;
        this.y = y;
        this.img = img;
        this.type = type;
        this.physic = true;
        this.direction = direction;

        this.isoSprite = new IsoSprite(x, y, img, obstacleGroup, this.physic);
        this.isoSprite.isoSprite.alpha = 0.8; // transparence
    }
    

    //_________________________________________________________________________________________
    getMapCoord() {
        const x = this.isoSprite.isoSprite.isoX;
        const y = this.isoSprite.isoSprite.isoY;
        const adjustment = 50;

        return {x: x + adjustment, y: y + adjustment};  // + 50 => âr rapport au décalage de coordonnée
    }


    //_________________________________________________________________________________________
    getTileCoord() {
        const x = this.getMapCoord().x / 200 >> 0;
        const y = this.getMapCoord().y / 200 >> 0;

        return {x: x, y: y};
    }


    //_________________________________________________________________________________________
    getSide() {
        const left = 1;
        const right = 2;
        const x = this.getMapCoord().x;

        return (x % tileSize < tileSize / 2) ? left : right ;
    }


    //_________________________________________________________________________________________
    update() {
        if (this.isoSprite.isoSprite.isoX < this.x - 5) // Trop haut
            this.isoSprite.isoSprite.body.velocity.x = (Math.abs(this.isoSprite.isoSprite.isoX - this.x)) * 0.75;
            
        else if (this.isoSprite.isoSprite.isoX > this.x + 5) // Trop bas
            this.isoSprite.isoSprite.body.velocity.x = - (Math.abs(this.isoSprite.isoSprite.isoX - this.x)) * 0.75;

        else if (this.isoSprite.isoSprite.body.velocity.x != 0) // OK & vitesse.x != 0
            this.isoSprite.isoSprite.body.velocity.x = 0;
    }

    
    //_________________________________________________________________________________________
    destoy() {
        // destroy

        this.isoSprite.destroy(); // Do IspSprite().destroy().call()
    }
    
}