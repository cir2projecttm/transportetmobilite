class Train extends Bot {
    //___________________________________________________________________________________________________________________________
    constructor(x, y, direction) {

        const img = (direction == `north`) ? `train0` : `train1`;
        const adjustment = tileSize * 0.85;
        x -= adjustment;
        y -= adjustment;

        super(x, y, img, direction,'train')
        
        this.set(direction);

        carIsoArray.push(this);
    }

    //___________________________________________________________________________________________________________________________
    set(direction) {

        let speed = tileSize;
        this.isoSprite.isoSprite.body.velocity.y = (direction == `north`) ? -speed : speed;
    }

    //___________________________________________________________________________________________________________________________
    destroy() {
        
        const foundIndex = carIsoArray.indexOf(this);
        carIsoArray.splice(foundIndex, 1) // Remove the isoSprite from carIsoArray

        super.destroy()
    }
}