//______________________________________________________________________________________________________________//
//______________________________________________________________________________________________________________//
class CrossRoad extends Frite {




    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------Constructors & initializations--------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    constructor(x, y) {
        super(x,y, `grass`);
        super.destroy()

        this.x = x;
        this.y = y;

        this.init();
        setTimeout(this.update.bind(this), randNum(8000))
    }


    //__________________________________________________________________________________________________________//
    init() {

        this.unrespectedCondition = false;

        this.status = { // Color according to direction
            NS: LightColor.green, // North - South
            EW: LightColor.red // Est - West
        };

        this.trafficLights = {
            north: this.newTrafficLight(Direction.north, LightColor.green), // North
            est:   this.newTrafficLight(Direction.est,   LightColor.red), // Est
            south: this.newTrafficLight(Direction.south, LightColor.green), // South
            west:  this.newTrafficLight(Direction.west,  LightColor.red) // West
        };

        for (let trafficLight of Object.values(this.trafficLights)) {
            game.physics.isoArcade.enable(trafficLight.isoSprite);
            trafficLight.isoSprite.body.collideWorldBounds = true;
            trafficLight.isoSprite.body.immovable = true;
        }
    }


    //__________________________________________________________________________________________________________//
    newTrafficLight(direction, color) {

        switch(direction) {
            case Direction.north:
                let xN = this.x - 1.100 * tileSize;
                let yN = this.y - 1.175 * tileSize;
                let trafficLightN = new IsoSprite(xN, yN, `trafficLight${direction}`, obstacleGroup); // North
                trafficLightN.isoSprite.animations.add(LightColor.green, [0], 1, true);
                trafficLightN.isoSprite.animations.add(LightColor.orange, [1], 1, true);
                trafficLightN.isoSprite.animations.add(LightColor.red, [2], 1, true);
                trafficLightN.isoSprite.animations.play(color);
                return trafficLightN;

            case Direction.est:
                let xE = this.x - 0.250 * tileSize;
                let yE = this.y - 1.175 * tileSize;
                let trafficLightE = new IsoSprite(xE, yE, `trafficLight${direction}`, obstacleGroup); // Est
                trafficLightE.isoSprite.animations.add(LightColor.green, [0], 1, true);
                trafficLightE.isoSprite.animations.add(LightColor.orange, [1], 1, true);
                trafficLightE.isoSprite.animations.add(LightColor.red, [2], 1, true);
                trafficLightE.isoSprite.animations.play(color);
                return trafficLightE;

            case Direction.south:
                let xS = this.x - 0.25 * tileSize;
                let yS = this.y - 0.30 * tileSize;
                let trafficLightS = new IsoSprite(xS, yS, `trafficLight${direction}`, obstacleGroup) // South
                trafficLightS.isoSprite.animations.add(LightColor.green, [0], 1, true);
                trafficLightS.isoSprite.animations.add(LightColor.orange, [1], 1, true);
                trafficLightS.isoSprite.animations.add(LightColor.red, [2], 1, true);
                trafficLightS.isoSprite.animations.play(color);
                return trafficLightS;

            case Direction.west:
                let xW = this.x - 1.1 * tileSize;
                let yW = this.y - 0.3 * tileSize;
                let trafficLightW = new IsoSprite(xW, yW, `trafficLight${direction}`, obstacleGroup) // West
                trafficLightW.isoSprite.animations.add(LightColor.green, [0], 1, true);
                trafficLightW.isoSprite.animations.add(LightColor.orange, [1], 1, true);
                trafficLightW.isoSprite.animations.add(LightColor.red, [2], 1, true);
                trafficLightW.isoSprite.animations.play(color);
                return trafficLightW;

            default:
                return undefined;
        }
    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------Updater-------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    update() {
        // If   orange => red
        // Elif red    => green
        // Else        => orange
        //console.log(`update`)

        if (this.status.NS == LightColor.green) {
            this.status.NS = LightColor.orange;
            this.trafficLights.north.isoSprite.animations.play(LightColor.orange);
            this.trafficLights.south.isoSprite.animations.play(LightColor.orange);
            setTimeout(this.update.bind(this), 2000);
        }
        else if (this.status.NS == LightColor.orange) {
            this.status.NS = LightColor.red;
            this.status.EW = LightColor.green;
            this.trafficLights.north.isoSprite.animations.play(LightColor.red);
            this.trafficLights.south.isoSprite.animations.play(LightColor.red);
            this.trafficLights.est.isoSprite.animations.play(LightColor.green);
            this.trafficLights.west.isoSprite.animations.play(LightColor.green);
            setTimeout(this.update.bind(this), 6000);
        }
        else if (this.status.EW == LightColor.green) {
            this.status.EW = LightColor.orange;
            this.trafficLights.est.isoSprite.animations.play(LightColor.orange);
            this.trafficLights.west.isoSprite.animations.play(LightColor.orange);
            setTimeout(this.update.bind(this), 2000);
        }
        else if (this.status.EW == LightColor.orange) {
            this.status.EW = LightColor.red;
            this.status.NS = LightColor.green;
            this.trafficLights.est.isoSprite.animations.play(LightColor.red);
            this.trafficLights.west.isoSprite.animations.play(LightColor.red);
            this.trafficLights.north.isoSprite.animations.play(LightColor.green);
            this.trafficLights.south.isoSprite.animations.play(LightColor.green);
            setTimeout(this.update.bind(this), 6000);
        }
    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------Entities conditions-------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    shouldYouStop(car) {
        // For bots
        //console.log("IN", car)

        const isoCar = car.isoSprite.isoSprite;

        if (this.status.NS != LightColor.green) {

            if (car.direction == Direction.north)
                if (isoCar.isoY < this.trafficLights.south.isoSprite.isoY + tileSize * 0.75) // check min distance : 100
                    return true;

            if (car.direction == Direction.south)
                if (isoCar.isoY > this.trafficLights.north.isoSprite.isoY - tileSize * 0.75)
                    return true;

        }

        else if (this.status.EW) {

            if (car.direction == Direction.est)
                if (isoCar.isoX > this.trafficLights.west.isoSprite.isoX - tileSize * 0.75)
                    return true;

            if (car.direction ==  Direction.west)
                if (isoCar.isoX < this.trafficLights.est.isoSprite.isoX + tileSize * 0.75)
                    return true;

        }

        return false;
    }


    //__________________________________________________________________________________________________________//
    isConditionRespected() {
        // For player

        if (!this.unrespectedCondition)
            if (this.status.NS == LightColor.red)
                if (player.isoY < this.trafficLights.south.isoSprite.isoY + tileSize * 0.3 && // check min distance : 50
                    player.isoY > this.trafficLights.north.isoSprite.isoY + tileSize * 0.3) {
                        this.unrespectedCondition = true;
                        return false;
                    }

        return true;
    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------Deconstructor----------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    destroy() {
        //super.destroy() already done in constructor()

        for (let trafficLight of Object.values(this.trafficLights))
            trafficLight.destroy();
    }
}
//______________________________________________________________________________________________________________//
//______________________________________________________________________________________________________________//
