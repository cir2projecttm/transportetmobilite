//______________________________________________________________________________________________________________//
//______________________________________________________________________________________________________________//
// Classe CrossingLine
class CrossingLine extends Frite {
    
    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------Constructors & initializations--------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    constructor(x, y, side=0) {
        super(x - 0.425 * tileSize,  y - 0.425 * tileSize, `crossingLine`, side);

        this.x = x;
        this.y = y;

        this.init(x, y);
        setTimeout(this.update.bind(this), randNum(8000));
    }


    //__________________________________________________________________________________________________________//
    init() {
        //

        this.unrespectedCondition = false;

        this.status = LightColor.green;

        // Set the character wich is waiting
        let img = !randNum(2) ? 'lilo' : 'girlwest1';
        this.waitingCharacter = new WaitingCharacter(this.x - 0.25 * tileSize, this.y - 0.60 * tileSize, img);


        this.trafficLights = {
            est:   this.newTrafficLight(Direction.est,   LightColor.red), // Est
            west:  this.newTrafficLight(Direction.west,  LightColor.red) // West
        };

        for (let trafficLight of Object.values(this.trafficLights)) {
            game.physics.isoArcade.enable(trafficLight.isoSprite);
            trafficLight.isoSprite.body.collideWorldBounds = true;
            trafficLight.isoSprite.body.immovable = true;
        }
    }


    //__________________________________________________________________________________________________________//
    newTrafficLight(direction, color) {

        switch(direction) {
            case Direction.est:
                let xE = this.x - 0.30 * tileSize;
                let yE = this.y - 0.50 * tileSize;
                let trafficLightE = new IsoSprite(xE, yE, `pedestrianLight${direction}`, obstacleGroup); // Est
                trafficLightE.isoSprite.animations.add(LightColor.green, [0], 1, true);
                trafficLightE.isoSprite.animations.add(LightColor.red, [1], 1, true);
                trafficLightE.isoSprite.animations.play(color);
                return trafficLightE;

            case Direction.west:
                let xW = this.x - 1.10 * tileSize;
                let yW = this.y - 0.80 * tileSize;
                let trafficLightW = new IsoSprite(xW, yW, `pedestrianLight${direction}`, obstacleGroup) // West
                trafficLightW.isoSprite.animations.add(LightColor.green, [0], 1, true);
                trafficLightW.isoSprite.animations.add(LightColor.red, [1], 1, true);
                trafficLightW.isoSprite.animations.play(color);
                return trafficLightW;

            default:
                return undefined;
        }
    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------Updater-------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    update() {
        // If   orange => red
        // Elif red    => green
        // Else        => orange
        //console.log(`update`)

        if (this.status == LightColor.green) {
            this.status = LightColor.red;
            this.trafficLights.est.isoSprite.animations.play(LightColor.red);
            this.trafficLights.west.isoSprite.animations.play(LightColor.red);
            setTimeout(this.update.bind(this), 8000);
        }
        else if (this.status == LightColor.red) {
            this.status = LightColor.green;
            this.trafficLights.est.isoSprite.animations.play(LightColor.green);
            this.trafficLights.west.isoSprite.animations.play(LightColor.green);
            setTimeout(this.update.bind(this), 5000);
        }

    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------Entities conditions-------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    shouldYouStop(car) {
        // For bots

        const isoCar = car.isoSprite.isoSprite;

        if (this.status == LightColor.green) {
            switch(car.direction) {
                case Direction.north:
                    if (isoCar.isoY < this.trafficLights.west.isoSprite.isoY + tileSize * 0.75) // check min distance : 100
                        return true;
                    break;

                case Direction.south:
                    if (isoCar.isoY > this.trafficLights.est.isoSprite.isoY - tileSize * 0.75)
                        return true;
                    break;
            }
        }
        return false;
    }


    //__________________________________________________________________________________________________________//
    shouldWeMoveLilo() {
        //

        let absIsoY = Math.abs(this.isoSprite.isoSprite.isoY);
        let absPlayerY = Math.abs(player.body.y);

        if (this.waitingCharacter.isWalking) this.waitingCharacter.startWalking();
        
        else if (this.status == LightColor.green && absPlayerY < absIsoY + 300)
            this.waitingCharacter.startWalking();

    }


    //__________________________________________________________________________________________________________//
    isConditionRespected() {
        // For player

        if (!this.unrespectedCondition)
            if (this.status == LightColor.green)
                if (player.isoY < this.trafficLights.est.isoSprite.isoY + tileSize * 0.3 && // check min distance : 50
                    player.isoY > this.trafficLights.west.isoSprite.isoY) {
                        this.unrespectedCondition = true;
                        return false;
                    }


        return true;
    }




    /*----------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------Deconstructor----------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------*/

    //__________________________________________________________________________________________________________//
    destroy() {
        super.destroy();

        // add destroy method of any isoSprite added

        for (let trafficLight of Object.values(this.trafficLights))
            trafficLight.destroy();

        this.waitingCharacter.isoSprite.destroy();
    }




    /*
    ça peut encore servir :

    let absIsoY = Math.abs(this.isoSprite.isoSprite.isoY);
    let absPlayerY = Math.abs(player.body.y);

    let absIsoX = Math.abs(this.isoSprite.isoSprite.isoX);
    let absPlayerX = Math.abs(player.body.x);



    if(absPlayerY < absIsoY+300 && absPlayerY > absIsoY
      && absPlayerX < absIsoX && absPlayerX > absIsoX - 500
       && this.waitingCharacter.isoSprite.isoSprite.isoX > this.waitingCharacter.destX)
    {
        //console.log("isoX,",this.waitingCharacter.isoSprite.isoSprite.isoX, 'destX', this.waitingCharacter.destX)
        this.waitingCharacter.startWalking();
    }

    if(absPlayerY < absIsoY+20 && absPlayerY > absIsoY && player.body.velocity.y != 0
      && absPlayerX < absIsoX + 50 && absPlayerX > absIsoX - 50){
        //console.log("You passed over a CrossingLine");
    }
    */
}
//______________________________________________________________________________________________________________//
//______________________________________________________________________________________________________________//
