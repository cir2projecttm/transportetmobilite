//———————————————————————————————————————————————————————————————————————————————————————————
// Classe TraficLight
class TraficLight extends Frite {
    //______________________________________________
    constructor(x, y,side = 0) {
        
        if(side == 2)
            super(x -  50,  y - 50, `trafficLight0`,side);
        
        if(side == 1)
            super(x -  tileSize*1.2,  y - tileSize*1.2, `trafficLight1`,side);


        // init color of trafic light
        switch(randNum(3)) {
            case 2:
                this.currentColor == 'green';
                break;
            case 1:
                this.currentColor == 'red';
                break;
            default: // Si 0 ou autre (erreur => feu défaillant)  => orange
                this.currentColor == 'orange';
        }
    }

    //______________________________________________
    updateColor(){
        switch(this.currentColor){
            case'green' :
                this.currentColor = 'orange'
                break;
            case'orange' :
                this.currentColor = 'red'
                break;
            case'red' :
                this.currentColor = 'green'
                break;
            default:
                this.currentColor = 'orange'
        }
    }

    //______________________________________________
    isConditionRespected(){
        
        let absIsoY = Math.abs(this.isoSprite.isoSprite.isoY);
        let absPlayerY = Math.abs(player.body.y);

        let absIsoX = Math.abs(this.isoSprite.isoSprite.isoX);
        let absPlayerX = Math.abs(player.body.x);

        
        if(absPlayerY < absIsoY+100 && absPlayerY > absIsoY && player.body.velocity.y != 0
          && absPlayerX < absIsoX  && absPlayerX > absIsoX - 200){
            //console.log("Going to pass a traficLight in ", Math.abs(absIsoY-absPlayerY));
        }

        if(absPlayerY < absIsoY+20 && absPlayerY > absIsoY && player.body.velocity.y != 0
          && absPlayerX < absIsoX && absPlayerX > absIsoX - 200){
            console.log("You passed a traficLight");
        }
    }

    //______________________________________________
    destroy() {
        // add destroy method of any isoSprite added

        super.destroy();
    }
}
