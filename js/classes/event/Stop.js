//———————————————————————————————————————————————————————————————————————————————————————————
// Classe Stop
class Stop extends Frite {
    //______________________________________________
    constructor(x, y, side = 0) {

        if(side == 2)//si panneau a droite de la route
            super(x - tileSize*0.2,  y , `stopSignsouth`,side);

        else if(side == 1)//si panneau en sens inverse
            super(x - tileSize*1.1,  y , `stopSignnorth`,side);

        this.init(x, y);
    }

    //______________________________________________
    init(x,y){
        this.isAlreadyDisrespected = false;
        this.remainingTimeToWait = 3000;
        this.isSomebodyWalking = (!randNum(2)) ? true : false;
        this._isStillArested = false;
        this.playerHasArrested = false;
    }


    //______________________________________________
    destroy() {
        super.destroy();
    }


    //______________________________________________
    //______________________________________________

    isStillArested(){
        this._isStillArested =  Math.abs(player.body.velocity.y) < 10 ? true : false;
    }
    
    distance(a,b){
        
        if(a>b)
            return Math.abs(a-b)
        else return Math.abs(b-a)
    }
    
    shouldYouStop(bot1){
        
        let absIsoY = Math.abs(this.isoSprite.isoSprite.isoY);
        let absbotY = Math.abs(bot1.isoSprite.isoSprite.isoY);

        /*
        if(this.side == bot1.getSide()){
            if(this.side == 1 && absbotY < absIsoY- 50 ){
                return true;
            }
            if(absbotY < absIsoY+50 && absbotY < absIsoY+48 && this.side == 2)
            {
                return true;
            }
        }
        */
        
        if(this.side == bot1.getSide() && this.distance(absIsoY, absbotY) < 10)
            return true;
            
        return false;
        
    }

    isConditionRespected(){


        let absIsoY = Math.abs(this.isoSprite.isoSprite.isoY);
        let absPlayerY = Math.abs(player.isoY);

        let absIsoX = Math.abs(this.isoSprite.isoSprite.isoX);
        let absPlayerX = Math.abs(player.isoX);

        //détecte si le player est dans la zone
        if(absPlayerY < absIsoY+300 && this.side == 2 && absPlayerX < absIsoX  && !this.isAlreadyDisrespected)
        {
            if(Math.abs(player.body.velocity.y) < 10 ){
                this.playerHasArrested = true;
            }
        }
        //si il a dépassé le stop
        if(!this.isAlreadyDisrespected && absPlayerY < absIsoY+20 && absPlayerY > absIsoY && this.side == 2){
            this.isAlreadyDisrespected = true;
            return this.playerHasArrested;
        }


        return true;

    }
}
