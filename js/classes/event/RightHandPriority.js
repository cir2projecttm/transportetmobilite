class RightHandPriority extends Frite {

    //___________________________________________________________________________________________________________________________
    constructor(x, y, status) {

        if (status == 'northsouth')
            super(x, y, `stopline${Direction.north}${Direction.south}`);
        else
            super(x, y, `stopline${Direction.est}${Direction.west}`);



        this.init(x, y, status)
    }


    //___________________________________________________________________________________________________________________________
    init(x, y, status) {
        //

        this.x = x;
        this.y = y;
        this.status = status;

        this.unrespectedCondition = false;
        this.doesPlayerStop = false;

        this.stopSigns = (this.status == Line.NS) ? {
            north: new IsoSprite(x - 1.100 * tileSize, y - 1.175 * tileSize, `stopSign${Direction.north}`, obstacleGroup), // North
            south: new IsoSprite(x - 0.250 * tileSize, y - 0.300 * tileSize, `stopSign${Direction.south}`, obstacleGroup) // South
        } : {
            est: new IsoSprite(x - 0.250 * tileSize, y - 1.175 * tileSize, `stopSign${Direction.est}`, obstacleGroup), // Est
            west: new IsoSprite(x - 1.100 * tileSize, y - 0.300 * tileSize, `stopSign${Direction.west}`, obstacleGroup) // West
        };

        for (let stopSign of Object.values(this.stopSigns)) {
            game.physics.isoArcade.enable(stopSign.isoSprite);
            stopSign.isoSprite.body.collideWorldBounds = true;
            stopSign.isoSprite.body.immovable = true; 
        }
    }
    

    //___________________________________________________________________________________________________________________________
    shouldYouStop(car) {
        if (this.status == Line.NS) {
            
            let absIsoY = Math.abs(this.stopSigns.south.isoSprite.isoY);
            let absbotY = Math.abs(car.isoSprite.isoSprite.isoY);

            //détecte si le bot est dans la zone
            if(absbotY < absIsoY+400)
                return true;
        }
        return false;
    }


    //___________________________________________________________________________________________________________________________
    isConditionRespected() {
        
        if (this.status == Line.NS)
            if (!this.unrespectedCondition && !this.doesPlayerStop) {
                if (player.isoY < this.stopSigns.south.isoSprite.isoY + tileSize * 0.2 && // check min distance : 50
                    player.isoY > this.stopSigns.north.isoSprite.isoY) {
                        this.unrespectedCondition = true;
                        return false;
                }
                else if (player.isoY < this.stopSigns.south.isoSprite.isoY + tileSize && 
                    player.isoY >= this.stopSigns.south.isoSprite.isoY + tileSize * 0.2 &&
                    Math.abs(player.body.velocity.y) < 10) {
                        this.doesPlayerStop = true;
                        return true;
                }
            }

        return true;
    }


    //___________________________________________________________________________________________________________________________
    destroy() {
        super.destroy()

        for (let stopSign of Object.values(this.stopSigns)) {
            stopSign.destroy();
        }
    }
}