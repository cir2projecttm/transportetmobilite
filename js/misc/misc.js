// generate random number
function randNum(num) {
    return Math.floor(Math.random() * num);
}

// Used to generate tiles => return the biome of the x th column
function getBiome(x) {
    for (let [areaKey, areaValue] of Object.entries(areas))
        if (areaValue.includes(x))
            for (let [biomeKey, biomeValue] of Object.entries(Biome))
                if (biomeKey == areaKey)
                    return biomeValue
    return -1;
}



//_______________________________________________________________
// Variables
let alreadyCrashedSomeone = false;
let isoMoved = 0; // Used to move each isosprite
const mid = (floorTileMapSize / 2 >> 0) - 1; // Define the main central road

let firstTime = true;

//_______________________________________________________________
// Array

let isoSpriteArray = new Array(); // Each isoSprite (used to move them when we generate the map)
let entitiesArray = new Array(); // Each entity (used to destroy them)
let carIsoArray = new Array(); // Cars
let floorTilesMap = Array.from( // 2D array of tiles class
    new Array(floorTileMapSize), () => Array.from(
        new Array(floorTileMapSize), () => undefined))



//________________________________________________________________
// Object()
// Define y column areas
let areas = {
    neutral: new Array(),
    road: new Array(),
    decor: new Array(),
    train: new Array(),
    river: new Array(),
    beach: new Array(),
    sea: new Array()
};


//_____________________________________________
// enum()
const Biome = { // Biomes
    neutral: 0,
    road: 1,
    decor: 2,
    train: 3,
    river: 4,
    beach: 5,
    sea: 6
};
const Physic = { // Physic of isoSprites
    disable: 0,
    obstacle: 1,
    entity: 2
}
const LightColor = { // Couleur de feu
    green: 'green',
    orange: 'orange',
    red: 'red'
}
const Direction = { // Directions
    north: 'north',
    est: 'est',
    south: 'south',
    west: 'west'
}
const Line = {
    NS: `${Direction.north}${Direction.south}`,
    EW: `${Direction.est}${Direction.west}`
}
Object.freeze(Biome);
Object.freeze(Physic);
Object.freeze(LightColor);
Object.freeze(Direction);
Object.freeze(Line);

console.log(Line)


//____________________________________________________________
function getTileCoord(x, y) {
    coordX = ((x + 50) / 200 >> 0);
    coordY = ((y + 50) / 200 >> 0);
    console.log(`=> tile[${coordX}][${coordY}]`)
}


//____________________________________________________________
function ConditionRespected(){

    if (player.body.velocity.y == 0) return; // n'actualise pas pour rien

    let playerY = (player.isoY + 50) / 200 >> 0;

    for (let y = playerY; y >= playerY - 1; y--) {
        if (!floorTilesMap[mid][y].isPlayerConditionRespected()) {

            if (floorTilesMap[mid][y]._event instanceof Stop) {
                console.log("Non-respect de l'arrêt au stop")
                showEventFailedInfo(game,"Non respect de l'arrêt au stop", 4, 135);
                ptsLicence -= 4;
                totalFine +=135;
                
            }

            else if (floorTilesMap[mid][y]._event instanceof CrossingLine) {
                console.log("Refus de priorité au passage piéton")
                showEventFailedInfo(game,"Non respect du feu de signalisation", 2, 120);
                totalFine +=120;
                ptsLicence -= 2;
            }
            
            else if (floorTilesMap[mid][y]._event instanceof CrossRoad) {
                console.log("carefour raté")
                showEventFailedInfo(game,"Non respect du feu de signalisation", 2, 120);
                totalFine +=120;
                ptsLicence -= 2;
            }
            
            else if (floorTilesMap[mid][y]._event instanceof RightHandPriority) {
                console.log("Non-respect de l'arrêt au stop")
                showEventFailedInfo(game, "Non-respect de l'arrêt au stop", 4, 135);
                //ptsLicence -= 2;
            }
        }
    }
}
