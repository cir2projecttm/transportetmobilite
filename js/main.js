
var init = function(color) {

	var BasicGame = function (game) { };

    BasicGame.Boot = function (game) { };

    BasicGame.Boot.prototype =
	{
        //____________________________________________________________________________________
	    preload: function () {

            //_____Load assets_______
            loadImages(game);

            loadSounds(game);

            // _____Game settings_____
            gameSettings(game);

        },

        //____________________________________________________________________________________
        create: function() {

            initTerrain(game, getBiome);

            initPlayer(game,color);

            initSound(game);

            initInput(game);

            initGUI(game);
        },

        //____________________________________________________________________________________
        update: function() {

            ConditionRespected();

            updateBotSpeed(game);

            didWeCrashedAnotherCar(game);
            
            didWeCrashedaPedestrian(game);

            tempUpdate(game, getBiome);
        },

        //____________________________________________________________________________________
        render: function() {
            tempRender(game);
            
           if (ptsLicence <= 0 && isGameOver == false)
                gameOver(game);
        }
        
        
    }

	game.state.add('Boot', BasicGame.Boot);
    game.state.start('Boot');
}
