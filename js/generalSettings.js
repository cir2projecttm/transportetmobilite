const width = window.innerWidth;
const height = window.innerHeight;
const size = (width > height) ? width : height;

// Tuile size
const tileSize = 200;

// Screen size (in number of tiles) (whole division)
const floorTileMapSize = (size * 2 / tileSize >> 0);

// Borders size in px
const mapSize = floorTileMapSize * 200;

// isoGroups
var floorGroup, onFloorGroup, obstacleGroup;
var game = new Phaser.Game(width, height, Phaser.AUTO, 'test', null, false, true);

// Scoring
var score = 0
var ptsLicence = 12
var distDone = 0
var totalFine = 0

// Speed
var cooldoownSpeedlimitCheck = true;
var listSpeedLimits = [50,70,90,110];
var currentSpeedLimit = 1

var isGameOver = false;


//tab
var waitingCharacterTab = new Array();