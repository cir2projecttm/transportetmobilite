function loadSounds(game){
    
    //fx
    soundPathArray = [
        'assets/sounds/GameSounds/gameStart.ogg','assets/sounds/GameSounds/menuSelection.ogg', 'assets/sounds/GameSounds/newZoneReached.ogg','assets/sounds/RoadFX/atmosTraffic1.ogg',
        'assets/sounds/RoadFX/atmosTraffic2.ogg','assets/sounds/RoadFX/blinkersOn.ogg',
        'assets/sounds/RoadFX/crash.ogg', 'assets/sounds/RoadFX/dubbed.ogg',
        'assets/sounds/RoadFX/emergencyVehicle.ogg', 'assets/sounds/RoadFX/hardBraking.ogg',
        'assets/sounds/RoadFX/klaxon2.ogg','assets/sounds/RoadFX/klaxon1.ogg',
        'assets/sounds/RoadFX/startingCar.ogg'
    ]

    soundNamesArray = [
        'gameStart','menuSelection','newZoneReached','atmosTraffic1','atmosTraffic2','blinkersOn',
        'crash','dubbed','emergencyVehicle','hardBraking','klaxon2','klaxon1','startingCar'
    ]

    for(let i = 0; i<soundNamesArray.length; i++)game.load.audio(soundNamesArray[i], soundPathArray[i]);
    
    //music
    game.load.audio('mainTheme', ['assets/sounds/Music/mainTheme.ogg', 'assets/sounds/Music/mainTheme.mp3']);
    
    game.load.audio('menu',  ['assets/sounds/Music/menu.ogg', 'assets/sounds/Music/menu.mp3']);
   
}