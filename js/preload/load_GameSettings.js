function gameSettings(game){

    game.time.advancedTiming = true;

    game.plugins.add(new Phaser.Plugin.Isometric(game)); // Add the Isometric plug-in to Phaser

    game.world.setBounds(0, 0, mapSize * 2, mapSize* 2); // Set the world size

    game.physics.startSystem(Phaser.Plugin.Isometric.ISOARCADE); // Start the physical system

    game.iso.anchor.setTo(0.5, 0.05); // set the anchor (origin) of the world in the top middle of the screen


    return;
}
