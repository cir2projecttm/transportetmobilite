function loadImages(game){
     // Floor tiles
    game.load.image('grass', 'assets/tiles/floor/grass.png'); // GRASS
    game.load.image('river', 'assets/tiles/floor/river.png'); // RIVER
    game.load.image('beach', 'assets/tiles/floor/beach.png'); // BEACH
    game.load.image('sea', 'assets/tiles/floor/sea.png'); // SEA
    for (let i = 0; i <= 4; i++) game.load.image(`road${i.toString()}`, `assets/tiles/floor/road${i.toString()}.png`); // ROAD
    for (let i = 0; i <= 1; i++) game.load.image(`trainTracks${i.toString()}`, `assets/tiles/floor/trainTracks${i.toString()}.png`); // TRAIN TRACKS
    for (let i = 0; i <= 9; i++) game.load.image(`building${i.toString()}`, `assets/tiles/building/building${i.toString()}.png`); // BUILDINGS
    for (let i = 0; i <= 10; i++) game.load.image(`decor${i.toString()}`, `assets/tiles/decor/decor${i.toString()}.png`); // DECORS


    // characters
    //easter egg
    game.load.image('stitch','assets/tiles/character/stitch.png');
    game.load.image('stitchsurf','assets/tiles/character/stitchsurf.png')

    game.load.spritesheet('girlnorth1','assets/tiles/character/girlnorth1.png', 25, 53); // PEDESTRIAN ANIMATION
    game.load.spritesheet('girlsouth1','assets/tiles/character/girlsouth1.png', 25, 53); // PEDESTRIAN ANIMATION
    game.load.spritesheet('girlsouth2','assets/tiles/character/girlsouth2.png', 25, 53); // PEDESTRIAN ANIMATION
    game.load.spritesheet('mansouth1','assets/tiles/character/mansouth1.png', 25, 53); // PEDESTRIAN ANIMATION


    //crossingLine
    game.load.spritesheet('lilo','assets/tiles/character/lilo.png', 25, 53); // CROSSLINE PEDESTRIAN ANIMATION
    game.load.spritesheet('girlwest1','assets/tiles/character/girlwest1.png', 25, 53); // CROSSLINE PEDESTRIAN ANIMATION



    // Entity tiles
    for (let i = 0; i <= 10; i++) game.load.image(`car${Direction.north}${i.toString()}`, `assets/tiles/car/car${Direction.north}${i.toString()}.png`); // NORTH CARS
    for (let i = 0; i <= 10; i++) game.load.image(`car${Direction.south}${i.toString()}`, `assets/tiles/car/car${Direction.south}${i.toString()}.png`); // SOUTH CARS
    for (let i = 0; i <= 1; i++) game.load.image(`train${i.toString()}`, `assets/tiles/train/train${i.toString()}.png`); // TRAINS
    for (let i = 0; i <= 3; i++) game.load.image(`boat${i.toString()}`, `assets/tiles/boat/boat${i.toString()}.png`); // BOATS


    // Obstacles tiles
    game.load.image('crossingLine', 'assets/tiles/floor/crossingLine.png'); // CROSSING LINE
    game.load.image('stoplineestwest', 'assets/tiles/floor/stoplineestwest.png'); // STOP LINE EW
    game.load.image('stoplinenorthsouth', 'assets/tiles/floor/stoplinenorthsouth.png'); // STOP LINE NS


    for (let location of Object.values(Direction))  game.load.image(`stopSign${location}`, `assets/tiles/trafficSign/stopSign${location}.png`); // STOP SIGN
    for (let location of Object.values(Direction)) game.load.spritesheet(`trafficLight${location}`, `assets/tiles/trafficSign/trafficLight${location}.png`, 15, 90); // TRAFIC LIGHT ANIMATION
    game.load.spritesheet(`pedestrianLight${Direction.est}`, `assets/tiles/trafficSign/pedestrianLight${Direction.est}.png`, 9, 50); // PEDESTRIAN LIGHT ANIMATION
    game.load.spritesheet(`pedestrianLight${Direction.west}`, `assets/tiles/trafficSign/pedestrianLight${Direction.west}.png`, 9, 50); // PEDESTRIAN LIGHT ANIMATION


    // GUI
    game.load.image('odometer', 'assets/GUI/odometer.png');
    game.load.image('needle', 'assets/GUI/needle.png');
    game.load.image('licence', 'assets/GUI/permis.png');
    game.load.image('fine', 'assets/GUI/amende.png');
    game.load.image('backToGame', 'assets/GUI/BackToGame.png');
    game.load.image('mainMenu', 'assets/GUI/mainMenu.png');

    game.load.image('speedLimit50', 'assets/GUI/speedLimit50.png');
    game.load.image('speedLimit70', 'assets/GUI/speedLimit70.png');
    game.load.image('speedLimit90', 'assets/GUI/speedLimit90.png');
    game.load.image('speedLimit110', 'assets/GUI/speedLimit110.png');


    // font
    WebFontConfig = {
        google: { families: ["Poppins"] }
        };
        (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
        })();

    return;
}
